<?php

namespace Madev\InpostParcelLockers\Plugin\Checkout\Model;

use Magento\Quote\Model\QuoteRepository;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement as BaseShippingInformationManagement;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Filter\FilterManager;

class ShippingInformationManagement
{
    /** @var QuoteRepository */
    protected $quoteRepository;

    /** @var CheckoutSession */
    protected $session;

    /**
     * @var FilterManager
     */
    protected $_filterManager;

    public function __construct(QuoteRepository $quoteRepository, CheckoutSession $session, FilterManager $filterManager)
    {
        $this->quoteRepository = $quoteRepository;
        $this->session = $session;
        $this->_filterManager = $filterManager;
    }

    /**
     * @param BaseShippingInformationManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @return null
     */
    public function beforeSaveAddressInformation(
        BaseShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    )
    {
        $extAttributes = $addressInformation->getExtensionAttributes();
        $parcelLocker = $extAttributes->getParcelLocker();

        $this->session->setParcelLocker($this->_filterManager->stripTags($parcelLocker));

        return null;
    }
}