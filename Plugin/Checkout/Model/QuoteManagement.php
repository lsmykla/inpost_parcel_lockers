<?php
/**
 * Copyright © 2019 Madev. All rights reserved.
 */

namespace Madev\InpostParcelLockers\Plugin\Checkout\Model;

use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Quote\Model\QuoteManagement as BaseQuoteManagement;

class QuoteManagement
{
    /**
     * @var HistoryFactory
     */
    protected $historyFactory;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /** @var CheckoutSession */
    protected $session;

    /**
     * QuoteManagement constructor.
     *
     * @param HistoryFactory $historyFactory
     * @param OrderFactory $orderFactory
     * @param CheckoutSession $session
     */
    public function __construct(
        HistoryFactory $historyFactory,
        OrderFactory $orderFactory,
        CheckoutSession $session
    )
    {
        $this->historyFactory = $historyFactory;
        $this->orderFactory = $orderFactory;
        $this->session = $session;
    }

    /**
     * @param BaseQuoteManagement $subject
     * @param int $orderId
     * @return int
     */
    public function afterPlaceOrder(BaseQuoteManagement $subject, $orderId = 0)
    {
        $parcelLocker = $this->session->getParcelLocker();

        if ($parcelLocker && strlen($parcelLocker) > 0) {
            /** @param OrderFactory $order */
            $order = $this->orderFactory->create()->load($orderId);

            if ($order->getEntityId()) {

                $comment = __('Parcel locker') . ': ' . $parcelLocker;

                /** @param string $status */
                $status = $order->getStatus();

                /** @param HistoryFactory $history */
                $history = $this->historyFactory->create();
                $history->setComment($comment);
                $history->setParentId($orderId);
                $history->setIsVisibleOnFront(1);
                $history->setIsCustomerNotified(0);
                $history->setEntityName('order');
                $history->setStatus($status);
                $history->save();
                $order->setCustomerNote($comment);
                $order->save();
                $this->session->setParcelLocker('');
            }
        }

        return $orderId;
    }
}