define([
        'jquery',
        'Magento_Checkout/js/model/quote',
        'mage/translate',
        'Magento_Ui/js/modal/alert',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/sidebar'
    ],
    function ($, quote, $t, magentoModal, stepNavigator, sidebarModel) {
        'use strict';
        return {
            /**
             * @returns {boolean}
             */
            validate: function () {

                var shippingMethod = quote.shippingMethod();

                if (!shippingMethod) {

                    this.backToShippingMethod($t('Please choose parcel locker!'), '');

                    return false;
                }

                var carrierCode = shippingMethod['carrier_code'],
                    parcelLockerCode = $('#parcel_locker').length === 1 ? $('#parcel_locker').val() : '';

                if (carrierCode === 'parcellocker' && parcelLockerCode.length === 0) {
                    this.backToShippingMethod($t('Please choose parcel locker!'), $t("Use button 'Select parcel locker' near selected shipping method and choose parcel locker from map or list."));

                    return false;
                }
                
                return true;
            },

            backToShippingMethod: function (title, content) {
                sidebarModel.hide();
                stepNavigator.navigateTo('shipping', 'opc-shipping_method');
                magentoModal({
                    title: title,
                    content: content
                });
            }
        }
    }
);