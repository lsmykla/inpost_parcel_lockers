/*global define,alert*/
define([
        'jquery',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/resource-url-manager',
        'mage/storage',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/model/payment/method-converter',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/action/select-billing-address',
        'mage/translate',
        'Magento_Ui/js/modal/alert'
    ], function ($,
                 ko,
                 quote,
                 resourceUrlManager,
                 storage,
                 paymentService,
                 methodConverter,
                 errorProcessor,
                 fullScreenLoader,
                 selectBillingAddressAction,
                 $t,
                 magentoModal) {
        'use strict';

        return {
            saveShippingInformation: function () {
                var payload;

                if (!quote.billingAddress()) {
                    selectBillingAddressAction(quote.shippingAddress());
                }

                var parcelLocker = $('#parcel_locker').val();

                if (quote.shippingMethod()['carrier_code'] === 'parcellocker' && parcelLocker.length === 0) {
                    fullScreenLoader.stopLoader();

                    magentoModal({
                        title: $t('Please choose parcel locker!'),
                        content: $t("Use button 'Select parcel locker' near selected shipping method and choose parcel locker from map or list."),
                    });

                    return false;
                }

                payload = {
                    addressInformation: {
                        'shipping_address': quote.shippingAddress(),
                        'billing_address': quote.billingAddress(),
                        'shipping_method_code': quote.shippingMethod()['method_code'],
                        'shipping_carrier_code': quote.shippingMethod()['carrier_code'],
                        'extension_attributes': {
                            'parcel_locker': $('#parcel_locker').val()
                        }
                    }
                };

                fullScreenLoader.startLoader();

                return storage.post(
                    resourceUrlManager.getUrlForSetShippingInformation(quote),
                    JSON.stringify(payload)
                ).done(
                    function (response) {
                        quote.setTotals(response.totals);
                        paymentService.setPaymentMethods(methodConverter(response['payment_methods']));
                        fullScreenLoader.stopLoader();
                    }
                ).fail(
                    function (response) {
                        errorProcessor.process(response);
                        fullScreenLoader.stopLoader();
                    }
                );
            }
        };
    }
);
