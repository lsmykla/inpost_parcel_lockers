define([
    'jquery',
    'ko',
    'uiComponent',
    'geowidgetSdk',
    'mage/translate'
], function ($, ko, Component, sdk, $t) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Madev_InpostParcelLockers/parcel-locker-block'
        },
        initialize: function () {
            this._super();

            $(document).ready(function () {
                $('#selected-parcel-locker-label').html($t('Selected parcel'));
                $('body').on('click', '.pudo-select-btn', function (event) {
                    event.preventDefault();
                    var $this = $(this);

                    $('.pudo-select-btn.selected').removeClass('selected');
                    $this.addClass('selected');

                    easyPack.init({
                        closeTooltip: false,
                        points: {
                            types: ['parcel_locker']
                        }
                    });

                    easyPack.modalMap(function (point) {
                        this.close();

                        var parcelLockerInput = $('#parcel_locker');

                        if (parcelLockerInput.length) {
                            parcelLockerInput.val(point.name);

                            $('.pudo-select-btn').html($t('Select parcel locker'));
                            $this.html($t('Change parcel'));

                            $('#selected-parcel-locker-data').html(point.address.line1 + ' <small>(' + point.name + ')</small>');
                            $('.selected-parcel-locker').show();
                        }

                    }, {width: 500, height: 600});

                    var overlay = $('#widget-modal__map').parent('div').parent('div');
                    overlay.css('background', 'rgba(0,0,0, 0.7)');
                });
            });

            return this;
        }
    });
});
