/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/step-navigator',
    'Magento_Checkout/js/model/sidebar'
], function ($, Component, quote, stepNavigator, sidebarModel) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Madev_InpostParcelLockers/shipping-information'
        },

        /**
         * @return {Boolean}
         */
        isVisible: function () {
            return !quote.isVirtual() && stepNavigator.isProcessed('shipping');
        },

        /**
         * @return {String}
         */
        getShippingMethodTitle: function () {
            var shippingMethod = quote.shippingMethod();

            return shippingMethod ? shippingMethod['carrier_title'] + ' ' + shippingMethod['method_title'] : '';
        },

        /**
         * @return {Boolean}
         */
        isParcelLocker: function () {
            var shippingMethod = quote.shippingMethod();

            if (!shippingMethod) {
                return false;
            }

            var carrierCode = shippingMethod['carrier_code'];

            return (carrierCode === 'parcellocker');
        },

        /**
         * @return {String}
         */
        getParcelLockerCode: function () {
            var parcelLockerCode = $('#parcel_locker').length === 1 ? $('#parcel_locker').val() : '';

            if (!this.isParcelLocker() || parcelLockerCode.length === 0) {
                return '';
            }

            return parcelLockerCode;
        },

        /**
         * Back step.
         */
        back: function () {
            sidebarModel.hide();
            stepNavigator.navigateTo('shipping');
        },

        /**
         * Back to shipping method.
         */
        backToShippingMethod: function () {
            sidebarModel.hide();
            stepNavigator.navigateTo('shipping', 'opc-shipping_method');
        }
    });
});
