define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/additional-validators',
        '../model/order-validator'
    ],
    function (Component, additionalValidators, orderValidator) {
        'use strict';
        additionalValidators.registerValidator(orderValidator);
        return Component.extend({});
    }
);