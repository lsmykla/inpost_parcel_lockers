var config = {
    "map": {
        "*": {
            'Magento_Checkout/js/model/shipping-save-processor/default': 'Madev_InpostParcelLockers/js/model/shipping-save-processor/default'
        }
    },
    paths: {
        "geowidgetSdk": [
            "Madev_InpostParcelLockers/js/lib/sdk-for-javascript"
        ]
    }
};