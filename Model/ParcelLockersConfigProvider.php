<?php

namespace Madev\InpostParcelLockers\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ParcelLockersConfigProvider implements ConfigProviderInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfiguration;

    /**
     * @param ScopeConfigInterface $scopeConfiguration
     * @codeCoverageIgnore
     */
    public function __construct(ScopeConfigInterface $scopeConfiguration)
    {
        $this->scopeConfiguration = $scopeConfiguration;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $showHide = ['show_hide_custom_block' => true];
        return $showHide;
    }
}